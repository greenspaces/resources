# Bristol One City
As a member of the city council, I want an app to give me environmental data about parks. This is so that I can review the data to improve quality.

# Potato Bristol
As a member of Potato, I want to create a high quality product so that I can satisfy my clients needs.

# Bristol city residents
As a Bristol resident, I want local parks to be as clean as possible so that I can be healthier.

# Student development Team
As a student working on the project, I want to perform well so that I can get a good grade. 

# Bristol University
As a University that is being represented, it is important that a good job is done to uphold reputation.
