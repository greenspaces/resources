# Requirements

The requirements for this project include a list of all stakeholders that will directly or indirectly interact with the system

## Stakeholders

1. Potato Bristol

   As the client for which we are creating a software solution, they are one of the main stakeholders of the project. They are a software company born out of Google and based in Bristol as well as having offices in London and San-Francisco.

   __User Stories__

   __Software Engineer at Potato__ _As a software engineer here at Potato it is my job to create a high quality product that will impress not only the client but also the department and my boss. I am committed to giving my clients what they want which is why making sure the work the students from the University of Bristol produce will meet the client's needs._

   __Head of Student Communications at Potato__ _As the person responsible for dealing with the interaction with the students from the university it is important to have regular meetings with them to ensure they know what we want them to achieve and make sure they are keeping in line with our agile development sprints. They will be required to perform presentations every meeting to demonstrate their progress._

   __Head of Client Communications at Potato__ _As the person in charge of talking directly to clients I need to be able to show them solid progress made by either the engineers or the students. The clients will want to know their investment is being used effectively and expect results to come hand in hand._

2. University of Bristol

   The university that is providing their students with real world experience and connecting Computer Science students with real world clients.

   __User Stories__

   __COMS20805 Unit Director__ _As unit director for this course, it is important I know the students are working to their full potential and achieving their own as well as their client's aims. It is my job to push them in the work they are doing to make sure they end up with a good grade for the unit._

   __UoB Computer Science department__ _For the department it is important our students respect the clients and uphold the reputation of the department as well as the reputation of the University. We don't want any complaints either from the client they are working for or any clients Potato might have the students doing work for._

   __UoB Vice Chancellor__ _As Vice-Chancellor I am in charge of making sure the university runs smoothly and our reputation and public image is upheld, it is therefore important for students interacting with non-university professionals to represent the university with great pride. We also look to appreciate students' work and therefore hope the students can produce something that may be put to use by their client._

3. Bristol City Council

   The end user client that Potato are working for as well as the governing body of Bristol. They will be responsible for determining the use of the project and who will use it in the future.

   __User Stories__

   __Bristol City Council: Manager for this project__ _As a client of Potato, while being in direct contact with them I need to make sure what is being delivered is in line with the project proposal. We must also find a use for it within the Council._

   __Charity End User of the Project__ _As a leader of a charity to combat homelessness in Bristol it is important that we can rely on this project to help the charity be more efficient and not be held up with a sudden interruption of the service in the moments we need it._

   __Bristol City Council: Outfacing Communications__ _As the person responsible for delivering the project to our consumers I need to be confident that the product will live up to users needs to make sure the reputation of the council stays as it is. This will ensure future business from any new projects._

4. Student Development Team

    The team of students behind the project who are the driving force to make sure the project runs smoothly, on time and with every member of the team helping equally to achieve.

   __User stories__

   __Tom Stark__ _During this project it will be my job to work on the app aspect of the implementation. I want to make sure I perform well and impress the clients but also work well in a team to make sure I can get a good grade. I am also passionate about this project being used in the real world and therefore will strive to fulfil my job within the group. I am also hopeful that this will be useful when it comes to future job interviews._

   __Gavin Wilkinson__ _As the group secretary, it is my job to ensure smooth communication both with our client and between members of our team. I want to make sure I can do this successfully so that our clients are happy with the project and our team can get a good mark._

   __Diego Lascurain__ _My goal is to produce a great website according to the specifications given by the client to serve them to the best of my ability._

   __Yuannan (Brandon) Lin__ _In our group my role is to assist the group where it is needed and then test the application to fit the specifications that the client demands._

## User Stories - Core set

We have taken the various user stories from above and formed a core set which we feel are the most important to the project's success.

__Potato Bristol:__
_Head of Student Communications_

We feel this is a vital user story for achieving the goal for this project as it is important they communicate well with us in order for us to know exactly what they want from us. The following steps illustrate how this goal can be achieved:

1. Clear and cohesive first meeting with the students to make sure they have a clear and achievable goal in mind, this gives them an idea of how much work is expected of them and what the overall aims are that our (Potato's) client wants.

2. Set up monthly meetings with the students that follow an agile development process.

3. Make sure we are always reachable by email or telephone for the students if they need help or are unclear on a certain aspect of the project.

4. Have students give presentations every meeting to demonstrate their progress and to evaluate whether they are on track or have gone down a wrong direction. It is important to notice this early to either steer students back down the right path or push them further if they are over achieving the sprint goals.

5. Have an end of project presentation where the students demonstrate all they have done over the teaching year and provide students with the opportunity to get a reference for their later career.

__University of Bristol__
_COMS20805 Unit Director_

This user story is also important as without the correct guidance from the unit director we may follow the client's needs without being in line with the assessment criteria, which may have a negative effect on the outcome of the unit for our group. These steps will ensure we won't have any problems completing the unit.

1. Adequately discuss the Intended Learning Outcomes (ILOs) for the unit right at the start with all the students to make sure they get a feel for what is expected of them over the coming year.

2. Make sure as many students as possible can be allocated a client of their liking through a fair process.

3. Give helpful lectures that could benefit the students in their journey of working with a real world client.

4. Have weekly meetings with the students in the Labs to address issues, give rewards to students that are striving and making sure each group is on track with the assessment criteria as well as meeting their clients' needs.

5. Have individual group meetings to discuss how the students are working as a group and gauge how much guidance the group might need.

6. Hold group presentations where they can show off their work so far. This can not only inspire other groups but also give us as unit directors an overall idea of the cohort's progress.

__Bristol City Council:__
_Charity End User_

This is an important user story as after the project is complete this is the user that will have the most contact with the finished product. Below are the steps a user is expected to take when using our services.

![flow](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/flow_normal.png "normal flow")

![flow](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/flow_exceptional.png "exceptional flow")

__Student Development Team__
_Whole Group_

As a group the best thing to do is work well as a group. This means good communication and clear leadership, as well as effective allocation of roles giving every person a fair chance to do work for the group and have an equal part. This will help us as a group to succeed and get a good grade using these steps:

1. Good communication between the team using an encrypted messaging service as well as the Jira dashboard where we can effectively allocate tasks.

2. Clear direction from the group leader to make sure we are on track and completing tasks on time.

3. Good interaction with the overseeing Mentor. We have an allocated mentor per group to whom we can ask questions or for advice as well as receive guidance from to lead us.

4. Good impressions when meeting the client. This is important not only to uphold the reputation of the university but also to have the project be as successful as possible.

5. Make sure we supply the client with a product they are satisfied with that follows their requirements. To do this it is important to follow the agile development timeline and make sure to ask the client any questions we have.

## User story requirements

We believe the most important user story is the charity end user from Bristol City Council. It is always important to please the end user of the product you are making, even if it is the client that is supplying the product who you need to make happy.

To reiterate, the steps for success for this user is to be able to navigate through the product (app and website) and to be able to fulfil their need for using the product we have supplied. We have broken down the flow of steps into a set of atomic requirements.

The functional requirements are the requirements needed to make the product work. Without these being fulfilled the end user will not have a successful experience with the product.

| Functional Requirement | Priority |
| :-- | :-- |
| User can enter data on app and it be added to database via API | High |
| Website can be refreshed to load new data from API (database) | High |
| API can connect to database  | High |
| Location and date data must be collected automatically  | Medium |
| Must show Bristol average statistics on app  | Medium |
| Must show ward specific statistics on app  | Low |

The non-functional requirements describes all the requirements that are not part of the functionality of the product. These are requirements that are not needed in order for the product to work but instead needed for the overall user experience and to satisfy the user.

| Non-Functional Requirement | Priority |
| :-- | :-- |
| Must be able to access website from anywhere | High |
| Must be able to access website at any time | High |
| Website must display correctly on at least 3 browsers | Low |
| App must work on at least 3 different types of Android phone | High |
| Must be able to submit data within 20s of opening app | Medium |
| Data entered should be checked locally to make sure it seems plausible before a user submits. This makes it easier to filter out unreasonable data in the database | Medium |
