# COMS2085: Software Product Engineering

## Portfolio B: Greenspaces

##### Tom Stark, Gavin Wilkinson, Diego Lascurain, Yuannan (Brandon) Lin

# Contents

- [Overview](#Overview)
  - [Project Proposal](##Project-proposal)
  - [Project Vision](##Project-visionl)
- [Requirements](#Requirements)
  - [Stakeholders](##Stakeholders)
  - [User Stories - Core Set](##User-Stories---Core-set)
  - [User Story Requirements](##User-story-requirements)
- [OO Design & UML](#OO-Design-&-UML)
  - [General Overview](##General-Overview)
  - [Storing the data in the database](##Storing-the-data-in-the-database)
  - [Detailed architecture view](##Detailed-architecture-view)
- [Development Testing](#Development-Testing)
- [Product Evaluation](#Product-Evaluation)
- [Client Documentation](#Client-Documentation)
- [Source Code](#Source-Code)

# Overview

The client we were allocated was from the _Greenspaces_ project called Potato who are a Software company born out of Google. They are based in Bristol but also have offices in London and San-Francisco. Their main purpose is developing software solutions for their clients all around the world dedicated to making the experience as smooth as possible.

## Project proposal

The initial brief was born from a concern that the data provided on the open data platform from the Bristol City Council was not very accessible, comprehensive or complete. The project was to give a new way to visualise data from this platform specifically relating to the housing situation in Bristol and how this may link to homelessness.

The idea was to display this 'hidden' data to be more accessible to the public so they can view data about the _ward_ they live in and would be able to compare data from their ward against the city average. This was to be displayed on an Android or web app.

We were invited to attend a Bristol City Council hackathon by our clients where the aim was to fully focus on homelessness in Bristol and come up with an effective way to help the situation. From then on our goal for the SPE project was to continue the project from the hackathon, building on the ideas formed.

The brief changed to focus on homelessness in Bristol and come up with a way to help charities or people who are actively trying to combat this issue. We have come up with a way to visualise the homelessness by drawing dots on a map to locate the homeless people in Bristol with the idea to give charities an opportunity to allocate resources and know where to send help.

This created another problem as there is no existing database to map homelessness in Bristol so the new brief included implementing a way to submit and add new data to a database as a mobile app. This app would be used either by charities or members of the public to submit homeless people they see on the street.

The data collected from the app should be sent to a relational database system on the Amazon Web Services platform which can then be accessed from a web app to plot and show the information collected. This includes data such as geo location and vague data about the person such as estimated age or gender which can then be viewed when hovering over the dot on the map.

## Project vision

The vision for the project was to have a working app and web app that seamlessly integrate and work together connected by an online database. This would mean data could be submitted at any time from anywhere via the mobile app and could be viewed straight away on the website in an automated process.

This would involve three separate services working together and interacting  and exchanging information without any issues.

1. An _Android App_ which would be a way to submit new data to the database, the data would need to be submitted in a universal format that interacts with the database.

2. A _Relational database_ which serves as a central node connecting our two services, compatibility between the platforms would be key to making it work.

3. Finally a _Web App_ to display the data gathered, pulling data from the database and displaying a marker on the map on the correct location together with other information gathered.

# Requirements

The requirements for this project include a list of all stakeholders that will directly or indirectly interact with the system

## Stakeholders

1. Potato Bristol

   As the client for which we are creating a software solution, they are one of the main stakeholders of the project. They are a software company born out of Google and based in Bristol as well as having offices in London and San-Francisco.

   __User Stories__

   __Software Engineer at Potato__ _As a software engineer here at Potato it is my job to create a high quality product that will impress not only the client but also the department and my boss. I am committed to giving my clients what they want which is why making sure the work the students from the University of Bristol produce will meet the client's needs._

   __Head of Student Communications at Potato__ _As the person responsible for dealing with the interaction with the students from the university it is important to have regular meetings with them to ensure they know what we want them to achieve and make sure they are keeping in line with our agile development sprints. They will be required to perform presentations every meeting to demonstrate their progress._

   __Head of Client Communications at Potato__ _As the person in charge of talking directly to clients I need to be able to show them solid progress made by either the engineers or the students. The clients will want to know their investment is being used effectively and expect results to come hand in hand._

2. University of Bristol

   The university that is providing their students with real world experience and connecting Computer Science students with real world clients.

   __User Stories__

   __COMS20805 Unit Director__ _As unit director for this course, it is important I know the students are working to their full potential and achieving their own as well as their client's aims. It is my job to push them in the work they are doing to make sure they end up with a good grade for the unit._

   __UoB Computer Science department__ _For the department it is important our students respect the clients and uphold the reputation of the department as well as the reputation of the University. We don't want any complaints either from the client they are working for or any clients Potato might have the students doing work for._

   __UoB Vice Chancellor__ _As Vice-Chancellor I am in charge of making sure the university runs smoothly and our reputation and public image is upheld, it is therefore important for students interacting with non-university professionals to represent the university with great pride. We also look to appreciate students' work and therefore hope the students can produce something that may be put to use by their client._

3. Bristol City Council

   The end user client that Potato are working for as well as the governing body of Bristol. They will be responsible for determining the use of the project and who will use it in the future.

   __User Stories__

   __Bristol City Council: Manager for this project__ _As a client of Potato, while being in direct contact with them I need to make sure what is being delivered is in line with the project proposal. We must also find a use for it within the Council._

   __Charity End User of the Project__ _As a leader of a charity to combat homelessness in Bristol it is important that we can rely on this project to help the charity be more efficient and not be held up with a sudden interruption of the service in the moments we need it._

   __Bristol City Council: Outfacing Communications__ _As the person responsible for delivering the project to our consumers I need to be confident that the product will live up to users needs to make sure the reputation of the council stays as it is. This will ensure future business from any new projects._

4. Student Development Team

    The team of students behind the project who are the driving force to make sure the project runs smoothly, on time and with every member of the team helping equally to achieve.

   __User stories__

   __Tom Stark__ _During this project it will be my job to work on the app aspect of the implementation. I want to make sure I perform well and impress the clients but also work well in a team to make sure I can get a good grade. I am also passionate about this project being used in the real world and therefore will strive to fulfil my job within the group. I am also hopeful that this will be useful when it comes to future job interviews._

   __Gavin Wilkinson__ _As the group secretary, it is my job to ensure smooth communication both with our client and between members of our team. I want to make sure I can do this successfully so that our clients are happy with the project and our team can get a good mark._

   __Diego Lascurain__ _My goal is to produce a great website according to the specifications given by the client to serve them to the best of my ability._

   __Yuannan (Brandon) Lin__ _In our group my role is to assist the group where it is needed and then test the application to fit the specifications that the client demands._

## User Stories - Core set

We have taken the various user stories from above and formed a core set which we feel are the most important to the project's success.

__Potato Bristol:__
_Head of Student Communications_

We feel this is a vital user story for achieving the goal for this project as it is important they communicate well with us in order for us to know exactly what they want from us. The following steps illustrate how this goal can be achieved:

1. Clear and cohesive first meeting with the students to make sure they have a clear and achievable goal in mind, this gives them an idea of how much work is expected of them and what the overall aims are that our (Potato's) client wants.

2. Set up monthly meetings with the students that follow an agile development process.

3. Make sure we are always reachable by email or telephone for the students if they need help or are unclear on a certain aspect of the project.

4. Have students give presentations every meeting to demonstrate their progress and to evaluate whether they are on track or have gone down a wrong direction. It is important to notice this early to either steer students back down the right path or push them further if they are over achieving the sprint goals.

5. Have an end of project presentation where the students demonstrate all they have done over the teaching year and provide students with the opportunity to get a reference for their later career.

__University of Bristol__
_COMS20805 Unit Director_

This user story is also important as without the correct guidance from the unit director we may follow the client's needs without being in line with the assessment criteria, which may have a negative effect on the outcome of the unit for our group. These steps will ensure we won't have any problems completing the unit.

1. Adequately discuss the Intended Learning Outcomes (ILOs) for the unit right at the start with all the students to make sure they get a feel for what is expected of them over the coming year.

2. Make sure as many students as possible can be allocated a client of their liking through a fair process.

3. Give helpful lectures that could benefit the students in their journey of working with a real world client.

4. Have weekly meetings with the students in the Labs to address issues, give rewards to students that are striving and making sure each group is on track with the assessment criteria as well as meeting their clients' needs.

5. Have individual group meetings to discuss how the students are working as a group and gauge how much guidance the group might need.

6. Hold group presentations where they can show off their work so far. This can not only inspire other groups but also give us as unit directors an overall idea of the cohort's progress.

__Bristol City Council:__
_Charity End User_

This is an important user story as after the project is complete this is the user that will have the most contact with the finished product. Below are the steps a user is expected to take when using our services.

![flow](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/flow_normal.png "normal flow")

![flow](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/flow_exceptional.png "exceptional flow")

__Student Development Team__
_Whole Group_

As a group the best thing to do is work well as a group. This means good communication and clear leadership, as well as effective allocation of roles giving every person a fair chance to do work for the group and have an equal part. This will help us as a group to succeed and get a good grade using these steps:

1. Good communication between the team using an encrypted messaging service as well as the Jira dashboard where we can effectively allocate tasks.

2. Clear direction from the group leader to make sure we are on track and completing tasks on time.

3. Good interaction with the overseeing Mentor. We have an allocated mentor per group to whom we can ask questions or for advice as well as receive guidance from to lead us.

4. Good impressions when meeting the client. This is important not only to uphold the reputation of the university but also to have the project be as successful as possible.

5. Make sure we supply the client with a product they are satisfied with that follows their requirements. To do this it is important to follow the agile development timeline and make sure to ask the client any questions we have.

## User Story Requirements

We believe the most important user story is the charity end user from Bristol City Council. It is always important to please the end user of the product you are making, even if it is the client that is supplying the product who you need to make happy.

To reiterate, the steps for success for this user is to be able to navigate through the product (app and website) and to be able to fulfil their need for using the product we have supplied. We have broken down the flow of steps into a set of atomic requirements.

The functional requirements are the requirements needed to make the product work. Without these being fulfilled the end user will not have a successful experience with the product.

| Functional Requirement | Priority |
| :-- | :-- |
| User can enter data on app and it be added to database via API | High |
| Website can be refreshed to load new data from API (database) | High |
| API can connect to database  | High |
| Location and date data must be collected automatically  | Medium |
| Must show Bristol average statistics on app  | Medium |
| Must show ward specific statistics on app  | Low |

The non-functional requirements describes all the requirements that are not part of the functionality of the product. These are requirements that are not needed in order for the product to work but instead needed for the overall user experience and to satisfy the user.

| Non-Functional Requirement | Priority |
| :-- | :-- |
| Must be able to access website from anywhere | High |
| Must be able to access website at any time | High |
| Website must display correctly on at least 3 browsers | Low |
| App must work on at least 3 different types of Android phone | High |
| Must be able to submit data within 20s of opening app | Medium |
| Data entered should be checked locally to make sure it seems plausible before a user submits. This makes it easier to filter out unreasonable data in the database | Medium |

# OO Design & UML

The clients' initial architecture request was for the solution to be an Android app focused around the Bristol Open Data platform. This would mean an Android architecture focused around making API requests to the Bristol Open Data platform and displaying it in a way that would make sense to an end user.

Our clients invited us to attend a hackathon organised by the Bristol City Council. From this hackathon our system took a different direction. This was possible because our real client was Bristol City Council. This enabled us to change our brief to a project that coincided with the goals of the hackathon we attended.

During the hackathon we developed a web based application that displayed multiple dots on a map of Bristol, depicting the locations of homeless people around Bristol. This changed our architecture over to a web based solution for which we chose Spring Boot, being heavily supported by the unit supervisors.

Once we had this in place we realised unless we wanted to display dummy data there had to be a way for us to capture our own data to use in our web app. This lead us back to the initial Android architecture and we developed an Android app to submit data to be displayed on the website.

## General Overview

The diagram below provides a high level overview of our solution. The arrows represent the transfer of data between each part. The text in each section gives a brief overview of how it will be implemented.

![hi_interaction](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/uml_1.png "high level interaction")

The main part of the diagram is the API. This bridges the data input from the app and the output on the website. This is the only component that has direct access to the database, allowing us to first sanitize any requests. It runs on an AWS EC2 instance meaning it can easily be accessed online by sending http requests.

The website also runs on the same EC2 instance and is integrated directly with the API but still requires the API methods for database access. To make it accessible the database is stored on an AWS RDS instance.

Also shown is the mobile application. This is only available for Android (as per client needs) and can write data to the database via the API. It also uses the API provided by the Bristol Open Data platform to load current data on housing and homelessness to be displayed.

## Storing the data in the database

In order to store the data in our database correctly we had to come up with all the data we wanted to collect and the relevant fields to make up our table.

| Person | |
| :-- | :-- |
| `Long`    | `id` |
| `Double`  | `latitude` |
| `Double`  | `longitude` |
| `Integer` | `height` |
| `Integer` | `age` |
| `Date`    | `date` |
| `String`  | `gender` |
| `String`  | `age` |

Above is the entity we used for storing each data point. Available methods have been excluded since these are just auto generated getters and setters. These attributes map directly with the fields stored in the database. Since this is the only data we need to store the database is very simple; containing just this single table.

These are the exact fields submitted from the app and also read by the website which means we have continuity throughout which makes the implementation of different architectures and bringing them all to work seamlessly together a lot easier.

## Detailed architecture view

If we take the high-level view shown above and break it down further we can see in-depth how the system operates at an architectural level.

![lo_interaction](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/uml_2.png "low level interaction")

Above is a representation of how each part of the program will interact with each other when a new data point is added. It is assumed the app and website have already been loaded. The leftmost flow simply collects the data and sends it out via a post request to the API. In the middle is the API, this waits for requests (posts from the app and gets from the website) and services them by accessing the database. After the data has been added, this diagram assumes the user then refreshes the website. A get request is sent and the display updated.

## Development Testing

The app required specific Android testing as the Android framework is unique and there are custom libraries to help with this.

To test the core aspects of the app, which include testing the codebase logic, we used _JUnit4_ to write a series of local unit tests to check that the code was doing what it was supposed to.

To test the UI elements of the App we made use of a library called _Espresso_ which lets you program user actions and test for an expected result. These were all integration tests because we had to make sure different views were working together within the fragment. These tests were useful in testing the input fields for the submission fragment that send the data to the database. Each input field needed to be filled in order to get an accurate and usable entry within the database so testing if a view had changed with an expected user input was crucial.

Testing the actual HTTP `POST` connection proved to be a challenge so a viable test seemed to be making sure the API we developed worked and could be submitted to using \emph{Postman} and then writing a series of unit tests within the app that checked the logic for the HTTP request worked as intended.

It was important for us to make sure the API was working correctly as this was the key component that linked the app and website together. This required testing the methods which read from and wrote to the database. Obviously, it would not be practical to use the actual database for this testing. Instead, a mock database was made using _Mockito_ and _Spring Boot_ annotations. Using these we were able to get 100% test coverage across both controllers and the model.

This just left the website itself to test. The controller it uses to get data has been tested meaning it is only the visual display that needs to be checked. This could not be tested automatically so instead we manually checked it displayed as intended using multiple different browsers and devices.

## Production Evaluation

Throughout development we decided on sprint goals and deadlines with our client. At the end of each sprint an evaluation was done by our clients to determine how successful we had been. Unfortunately due to COVID-19 we were unable to have a final meeting to do a final evaluation on the product. Additionally, our clients were in the process of contacting some charities so they could evaluate the utility of our project during this time. Again this was not possible to set up in the end.

We planned on asking some people to download our app to submit information to the database and use our website to view it. This would allow us to observe their experience and survey them to see if our non-functional requirements were met and take user suggestions. The evaluation would entail:

1. Observing how quickly a first time user can understand the interface and interact with it.

2. Observing whether users clearly understand submission options on their first attempt.

3. Observing which app functionalities the users are drawn to the most.

4. Asking the participant to rate their experience on a Likert scale with options ranging from "Very unenjoyable" to "Very enjoyable"

5. Asking the participant which aspects of the app/website can be improved.

6. Asking the participant what features they would like to see in this software.

We were not able to coordinate this. As a result the best final review we did was to manually test against the success criteria.

# Client Documentation

This section aims to demonstrate how the product works and how it can be used by providing a series of screenshots.

![home](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/home.jpg "home page")
![ward](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/ward.jpg "ward view page")

The above two screenshots show the first two pages of the app. These simply show the data taken from the Bristol Open Data platform.

![map](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/map.jpg "map page")
![mapHover](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/mapHover.jpg "details given on tap")

The page shown above contains an embedded version of the map available on the website. This will update to show all new data points. An image of the full website is shown further below.

![input](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/input.jpg "blank form")
![inputted](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/inputted.jpg "filled form")
![confirm](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/confirm.jpg "confirmation message")
![thanks](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/thanks.jpg "confirm message sent")

Finally, these 4 screenshots above show the data input page with sample input and confirmation messages. This page is the most important part of the app and the main reason for its existence. Of the data collected, the location is the most important. This is taken automatically using the 'get location' button.

![map](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/fullMap.jpg "Full Size Web View")

The data displayed on the map will update with any new data when refreshed. This data is stored in a single database which can only be accessed via the built in API. A sample of this table is shown below.

![database](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/database.jpg "Database Table")

As mentioned access to this database is only available via an API. The relevant requests are listed below and can be used to manually change the data stored. All paths are given relative to the website address.

| Method | Path | Description |
| :--- | :--- | :--- |
| `GET` | `/persons` | Returns a list of all people |
| `GET` | `/persons/{id}` | Returns details of a specific person |
| `POST` | `/persons` (giving JSON data) | Add a new person giving details in JSON form |
| `DELETE` | `/persons{id}` | Delete person with given ID |

# Source Code

We split our project into separate repos, one for each aspect of the design

<https://bitbucket.org/greenspaces/>
