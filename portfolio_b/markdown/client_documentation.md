# Client Documentation

This section aims to demonstrate how the product works and how it can be used by providing a series of screenshots.

![home](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/home.jpg "home page")
![ward](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/ward.jpg "ward view page")

The above two screenshots show the first two pages of the app. These simply show the data taken from the Bristol Open Data platform.

![map](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/map.jpg "map page")
![mapHover](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/mapHover.jpg "details given on tap")

The page shown above contains an embedded version of the map available on the website. This will update to show all new data points. An image of the full website is shown further below.

![input](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/input.jpg "blank form")
![inputted](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/inputted.jpg "filled form")
![confirm](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/confirm.jpg "confirmation message")
![thanks](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/thanks.jpg "confirm message sent")

Finally, these 4 screenshots above show the data input page with sample input and confirmation messages. This page is the most important part of the app and the main reason for its existence. Of the data collected, the location is the most important. This is taken automatically using the 'get location' button.

![map](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/fullMap.jpg "Full Size Web View")

The data displayed on the map will update with any new data when refreshed. This data is stored in a single database which can only be accessed via the built in API. A sample of this table is shown below.

![database](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/database.jpg "Database Table")

As mentioned access to this database is only available via an API. The relevant requests are listed below and can be used to manually change the data stored. All paths are given relative to the website address.

| Method | Path | Description |
| :--- | :--- | :--- |
| `GET` | `/persons` | Returns a list of all people |
| `GET` | `/persons/{id}` | Returns details of a specific person |
| `POST` | `/persons` (giving JSON data) | Add a new person giving details in JSON form |
| `DELETE` | `/persons{id}` | Delete person with given ID |
| `PUT` | `/persons{id}` (giving JSON data) | Update (overwrite) person with given ID |

