## Production Evaluation

Throughout development we decided on sprint goals and deadlines with our client. At the end of each sprint an evaluation was done by our clients to determine how successful we had been. Unfortunately due to COVID-19 we were unable to have a final meeting to do a final evaluation on the product. Additionally, our clients were in the process of contacting some charities so they could evaluate the utility of our project during this time. Again this was not possible to set up in the end.

We planned on asking some people to download our app to submit information to the database and use our website to view it. This would allow us to observe their experience and survey them to see if our non-functional requirements were met and take user suggestions. The evaluation would entail:

1. Observing how quickly a first time user can understand the interface and interact with it.

2. Observing whether users clearly understand submission options on their first attempt.

3. Observing which app functionalities the users are drawn to the most.

4. Asking the participant to rate their experience on a Likert scale with options ranging from "Very unenjoyable" to "Very enjoyable"

5. Asking the participant which aspects of the app/website can be improved.

6. Asking the participant what features they would like to see in this software.

We were not able to coordinate this. As a result the best final review we did was to manually test against the success criteria.
