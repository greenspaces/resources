# OO Design & UML

The clients' initial architecture request was for the solution to be an Android app focused around the Bristol Open Data platform. This would mean an Android architecture focused around making API requests to the Bristol Open Data platform and displaying it in a way that would make sense to an end user.

Our clients invited us to attend a hackathon organised by the Bristol City Council. From this hackathon our system took a different direction. This was possible because our real client was Bristol City Council. This enabled us to change our brief to a project that coincided with the goals of the hackathon we attended.

During the hackathon we developed a web based application that displayed multiple dots on a map of Bristol, depicting the locations of homeless people around Bristol. This changed our architecture over to a web based solution for which we chose Spring Boot, being heavily supported by the unit supervisors.

Once we had this in place we realised unless we wanted to display dummy data there had to be a way for us to capture our own data to use in our web app. This lead us back to the initial Android architecture and we developed an Android app to submit data to be displayed on the website.

## General Overview

The diagram below provides a high level overview of our solution. The arrows represent the transfer of data between each part. The text in each section gives a brief overview of how it will be implemented.

![hi_interaction](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/uml_1.png "high level interaction")

The main part of the diagram is the API. This bridges the data input from the app and the output on the website. This is the only component that has direct access to the database, allowing us to first sanitize any requests. It runs on an AWS EC2 instance meaning it can easily be accessed online by sending http requests.

The website also runs on the same EC2 instance and is integrated directly with the API but still requires the API methods for database access. To make it accessible the database is stored on an AWS RDS instance.

Also shown is the mobile application. This is only available for Android (as per client needs) and can write data to the database via the API. It also uses the API provided by the Bristol Open Data platform to load current data on housing and homelessness to be displayed.

## Storing the data in the database

In order to store the data in our database correctly we had to come up with all the data we wanted to collect and the relevant fields to make up our table.

| Person | |
| :-- | :-- |
| `Long`    | `id` |
| `Double`  | `latitude` |
| `Double`  | `longitude` |
| `Integer` | `height` |
| `Integer` | `age` |
| `Date`    | `date` |
| `String`  | `gender` |
| `String`  | `age` |

Above is the entity we used for storing each data point. Available methods have been excluded since these are just auto generated getters and setters. These attributes map directly with the fields stored in the database. Since this is the only data we need to store the database is very simple; containing just this single table.

These are the exact fields submitted from the app and also read by the website which means we have continuity throughout which makes the implementation of different architectures and bringing them all to work seamlessly together a lot easier.

## Detailed architecture view

If we take the high-level view shown above and break it down further we can see in-depth how the system operates at an architectural level.

![lo_interaction](https://bitbucket.org/greenspaces/resources/raw/master/portfolio_b/latex/images/uml_2.png "low level interaction")

Above is a representation of how each part of the program will interact with each other when a new data point is added. It is assumed the app and website have already been loaded. The leftmost flow simply collects the data and sends it out via a post request to the API. In the middle is the API, this waits for requests (posts from the app and gets from the website) and services them by accessing the database. After the data has been added, this diagram assumes the user then refreshes the website. A get request is sent and the display updated.
