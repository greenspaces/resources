\ifdefined\booklet
\else
\documentclass{article}
\include{header}
\begin{document}
\fi
\section{OO Design \& UML}
The clients' initial architecture request was for the solution to be an Android app focused around the Bristol Open Data platform. This would mean an Android architecture focused around making API requests to the Bristol Open Data platform and displaying it in a way that would make sense to an end user.

Our clients invited us to attend a hackathon organised by the Bristol City Council. From this hackathon our system took a different direction. This was possible because our real client was Bristol City Council. This enabled us to change our brief to a project that coincided with the goals of the hackathon we attended.

During the hackathon we developed a web based application that displayed multiple dots on a map of Bristol, depicting the locations of homeless people around Bristol. This changed our architecture over to a web based solution for which we chose Spring Boot, being heavily supported by the unit supervisors.

Once we had this in place we realised unless we wanted to display dummy data there had to be a way for us to capture our own data to use in our web app. This lead us back to the initial Android architecture and we developed an Android app to submit data to be displayed on the website.

\subsection{General Overview}
The diagram below provides a high level overview of our solution. The arrows represent the transfer of data between each part. The text in each section gives a brief overview of how it will be implemented.
\begin{figure}[H]
\begin{center}
    \begin{tikzpicture}[>=stealth']
        \node[draw,cloud,aspect=2.5,cloud puffs=15,inner sep=0mm,cloud puff arc=110] (a) at (-1,0) {\begin{tabular}{c}
            Bristol\\
            Open Data\\
            External
        \end{tabular}};
        \node at (0,-4) {\begin{tabular}{c}
            App\\
            Android
        \end{tabular}};
        \node[draw,cylinder,shape border rotate=90,aspect=0.25] (b) at (4,-4) {\begin{tabular}{c}
            Database\\
            AWS\\
            RDS
        \end{tabular}};
        \node[draw,cloud,aspect=2.5,cloud puffs=15,inner sep=0mm,cloud puff arc=110] (c) at (4,0) {\begin{tabular}{c}
            API\\
            AWS EC2
        \end{tabular}};
        \node[draw,cloud,aspect=2.5,cloud puffs=15,inner sep=0mm,cloud puff arc=110] (d) at (8,0) {\begin{tabular}{c}
            Website\\
            AWS EC2
        \end{tabular}};

        % drawing the phone
        \draw[rounded corners=1.5mm] (-1,-2.25) rectangle (1,-5.75);
        \draw[rounded corners=0.3mm] (-0.2,-2.335) rectangle (0.2,-2.375);
        \draw[rounded corners=0.5mm] (-0.9,-2.45) rectangle (0.9,-5.55);
        %\draw (0,-5.5) circle (1.5mm);

        %connecting the nodes
        \draw[->] (a) to (0, -2.2);
        \draw[->] (1, -2.2) to (c);
        \draw[->] (3.5,-0.8) to (3.5,-3);
        \draw[->] (4.5,-3) to (4.5,-0.8);
        \draw[->] (c) to (d);
    \end{tikzpicture}
\end{center}
    \caption{High level overview}
\end{figure}
The main part of the diagram is the API. This bridges the data input from the app and the output on the website. This is the only component that has direct access to the database, allowing us to first sanitize any requests. It runs on an AWS EC2 instance meaning it can easily be accessed online by sending http requests.

The website also runs on the same EC2 instance and is integrated directly with the API but still requires the API methods for database access. To make it accessible the database is stored on an AWS RDS instance.

Also shown is the mobile application. This is only available for Android (as per client needs) and can write data to the database via the API. It also uses the API provided by the Bristol Open Data platform to load current data on housing and homelessness to be displayed.

\subsection{Storing the data in the database}
In order to store the data in our database correctly we had to come up with all the data we wanted to collect and the relevant fields to make up our table.

\begin{figure}[H]
\begin{table}[H]
    \centering
    \renewcommand{\arraystretch}{1.2}
    \begin{tabular}{|p{4cm}|}
        \hline
        \multicolumn{1}{|c|}{Person}\\\hline
        \texttt{Long id}\\
        \texttt{Double latitude}\\
        \texttt{Double longitude}\\
        \texttt{Integer height}\\
        \texttt{Integer age}\\
        \texttt{Date date}\\
        \texttt{String Gender}\\
        \texttt{String Info}\\\hline
    \end{tabular}
\end{table}
    \caption{Person entity}
\end{figure}
Above is the entity we used for storing each data point. Available methods have been excluded since these are just auto generated getters and setters. These attributes map directly with the fields stored in the database. Since this is the only data we need to store the database is very simple; containing just this single table.

These are the exact fields submitted from the app and also read by the website which means we have continuity throughout which makes the implementation of different architectures and bringing them all to work seamlessly together a lot easier.

\subsection{Detailed architecture view}
If we take the high-level view shown above and break it down further we can see in-depth how the system operates at an architectural level.
\begin{figure}[H]
\begin{center}
    \footnotesize
    \begin{tikzpicture}[yscale=0.8,>=stealth']
        \draw (-0.75,1) rectangle (3.25, -9);
        \node at (1.25,1.25) {App};
        \draw (-0.75,1.5) rectangle (3.25, 1);
        \draw (3.25,1) rectangle (8.75, -9);
        \node at (6,1.25) {API};
        \draw (3.25,1.5) rectangle (8.75, 1);
        \draw (8.75,1) rectangle (13.75, -9);
        \node at (11.25,1.25) {Web};

        \draw (8.75,1.5) rectangle (13.75, 1);

        \node (a1) at (1.25,0) {idle};
        \draw (0.25,0.5) rectangle (2.25,-0.5);
        \node (a2) at (1.25,-2) {\begin{tabular}{c}
            user submits\\
            new data
        \end{tabular}};
        \draw (0.25,-1.5) rectangle (2.25,-2.5);
        \node (a3) at (1.25,-4) {\begin{tabular}{c}
            send post\\
            request
        \end{tabular}};
        \draw (0.25,-3.5) rectangle (2.25,-4.5);
        \node (a3) at (1.25,-6) {\begin{tabular}{c}
            user enters\\
            wrong info
        \end{tabular}};
        \draw (0.25,-5.5) rectangle (2.25,-6.5);
        \node (b1) at (5,0) {idle};
        \draw (4,0.5) rectangle (6,-0.5);
        \node (b2) at (5,-4) {\begin{tabular}{c}
            receive post\\
            request
        \end{tabular}};
        \draw (4,-3.5) rectangle (6,-4.5);
        \draw (6.5,-3.5) rectangle (8.5,-4.5);  
        \node (b21) at (7.5,-4) {\begin{tabular}{c}
            add to\\
            database
        \end{tabular}};
        \draw (4,-5.5) rectangle (6,-6.5);
        \node (b3) at (5,-6) {\begin{tabular}{c}
            receive get\\
            request
        \end{tabular}};
        \draw (4,-7.5) rectangle (6,-8.5);
        \node (b4) at (5,-8) {\begin{tabular}{c}
            read from\\
            database
        \end{tabular}};
        \draw (9,0.5) rectangle (11,-0.5);
        \node (c1) at (10,0) {idle};
        \draw (9,-3.5) rectangle (11,-4.5);
        \node (c2) at (10,-4) {\begin{tabular}{c}
            user refreshes\\
            page
        \end{tabular}};
        \draw (9,-5.5) rectangle (11,-6.5);
        \node (c3) at (10,-6) {\begin{tabular}{c}
            send get\\
            request
        \end{tabular}};
        \draw (9,-7.5) rectangle (11,-8.5);
        \node (c4) at (10,-8) {\begin{tabular}{c}
            receive new\\
            data
        \end{tabular}};
        \draw (11.5,-7.5) rectangle (13.5,-8.5);
        \node (c41) at (12.5,-8) {\begin{tabular}{c}
            update\\
            display
        \end{tabular}};
        
        \draw[->] (1.25,-0.5) to (1.25,-1.5);
        \draw[->] (2.25,-2) -| ++(0.5,0) |- (2.25,-6);
        \draw[->] (0.25,-4) -| ++(-0.25,0) |- (0.25,-0.1);
        \draw[->] (0.25,-6) -| ++(-0.5,0) |- (0.25,0.1);
        \draw[->] (2.25,-4) to (4,-4);
        \draw[->] (4,-0.1) -| ++(-0.25,0) |- (4,-6);
        \draw[->] (4,-8) -| ++(-0.5,0) |- (4,0.1);
        \draw[->] (5,-0.5) to (5,-3.5);
        \draw[->] (6,-4) to (6.5,-4);
        \draw[->] (7.5,-3.5) -| ++(0,0) |- (6,0);
        \draw[->] (6,-8) to (9,-8);
        \draw[->] (9,-6) to (6,-6);
        \draw[->] (5,-6.5) to (5,-7.5);
        \draw[->] (10,-0.5) to (10,-3.5);
        \draw[->] (10,-4.5) to (10,-5.5);
        \draw[->] (11,-8) to (11.5,-8);
        \draw[->] (12.5,-7.5) -| ++(0,0) |- (11,0);
    \end{tikzpicture}
\end{center}
    \caption{Adding a new data point}
\end{figure}
Above is a representation of how each part of the program will interact with each other when a new data point is added. It is assumed the app and website have already been loaded. The leftmost flow simply collects the data and sends it out via a post request to the API. In the middle is the API, this waits for requests (posts from the app and gets from the website) and services them by accessing the database. After the data has been added, this diagram assumes the user then refreshes the website. A get request is sent and the display updated.
\ifdefined\booklet
\else
\end{document}
\fi
